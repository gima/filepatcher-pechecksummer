/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string.h>

#include "Commons.h"


int ParsePatchFileLine( char* rpBufLine, int riLineLength, unsigned int* riPatchOffset, char** rpPatchCheck,
  char** rpPatch, int* rpPatchLength )
{

  int liError = ERR_OK;

  // Get first components position in buf
  int liFirstPos = (int)strstr( rpBufLine, "," );
  if ( liFirstPos ==  0 )
  {
    liError = ERR_NO_FIRST_COMPONENT;
    goto _Finished;
  }
  liFirstPos -= (int)rpBufLine;


  // Get second components position in buf
  if ( liFirstPos+1 >= (riLineLength-2) )
  {
    liError = ERR_FIRSTCOMPONENT_LASTCOMPONENT;
    goto _Finished;
  }

  int liSecondPos = (int)strstr( &rpBufLine[ liFirstPos+1 ], "," );
  if ( liSecondPos == 0 )
  {
    liError = ERR_NO_SECOND_COMPONENT;
    goto _Finished;
  }
  liSecondPos -= (int)rpBufLine;


  // Get third components position in buf
  if ( liSecondPos+1 >= riLineLength )
  {
    liError = ERR_SECONDCOMPONENT_LASTCOMPONENT;
    goto _Finished;
  }

  int liThirdPos = riLineLength;

  if ( ( liFirstPos+1 == liSecondPos ) || ( liSecondPos+1 == liThirdPos ) )
  {
    liError = ERR_EMPTY_COMPONENT;
    goto _Finished;
  }


  // Correct positions
  rpBufLine[ liThirdPos ] = 0;

  liThirdPos = liSecondPos+1;
  rpBufLine[ liSecondPos ] = 0;

  liSecondPos = liFirstPos+1;
  rpBufLine[ liFirstPos ] = 0;

  liFirstPos = 0;



  int liFirstLen;
  int liSecondLen;
  int liThirdLen;
  liFirstLen  = strlen( &rpBufLine[ liFirstPos ] );
  liSecondLen = strlen( &rpBufLine[ liSecondPos ] );
  liThirdLen  = strlen( &rpBufLine[ liThirdPos ] );

  // Lengths of each component must be power of two
  if ( ( liFirstLen % 2 != 0 ) || ( liSecondLen % 2 != 0 ) || ( liThirdLen % 2 != 0 ) )
  {
    liError = ERR_PATCHES_NOT_MOD2;
    goto _Finished;
  }

  // Patch and patch check must be of same length
  if ( liSecondLen != liThirdLen )
  {
    liError = ERR_CHECK_PATCH_DIFFERENT_LENGTH;
    goto _Finished;
  }


  // Get patch offset from "string" at FirstPos
  sscanf( &rpBufLine[ liFirstPos ], "%x", riPatchOffset );

  // As patches are in text, FF is two chars but only one byte, thus assign half the length of memory for buffer to hold the patches
  *rpPatchLength = liSecondLen / 2;
  *rpPatchCheck  = new char[ *rpPatchLength ];
  *rpPatch       = new char[ *rpPatchLength ];

  // Convert text patches to buffer bytes
  ConvertTextToBuf( *rpPatchCheck, &rpBufLine[ liSecondPos ], *rpPatchLength );
  ConvertTextToBuf( *rpPatch,      &rpBufLine[ liThirdPos ],  *rpPatchLength );

  _Finished:


  return liError;

}


