/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Commons.h"


unsigned int FileGetSize( FILE* rpFile )
{
  // Save original file position, seek to end, save it(x), seek to original, return x
  int liOriginalPos;
  liOriginalPos = ftell( rpFile );

  int liSize;
  fseek( rpFile, 0, SEEK_END );
  liSize = ftell( rpFile );

  fseek( rpFile, liOriginalPos, SEEK_SET );

  return liSize;
}


int FileTransferData( FILE* rpFile, char* rpBuf, int riSize, int riTransferMode )
{

  int liTransferred         = 0;
  int liTotallTransferred  = 0;

  // Repeat read/write until no more stuff is transferred (or error)
  while ( 1 )
  {

    switch ( riTransferMode )
    {
      case TRANSFER_READ:
        liTransferred = fread( &rpBuf[ liTotallTransferred ], 1, riSize - liTotallTransferred, rpFile );
        break;
      case TRANSFER_WRITE:
        liTransferred = fwrite( &rpBuf[ liTotallTransferred ], 1, riSize - liTotallTransferred, rpFile );
        break;
    }

    if ( liTransferred == 0 )
    {
      break;
    }

    liTotallTransferred += liTransferred;

  } // while 1

  // If total file was not read, its an error
  if ( liTotallTransferred != riSize )
  {
    return ERR_NOT_ENOUGH_TRANSFERRED;
  }

  return ERR_OK;
}


int PatchFile( FILE* rpFile, unsigned int riFileSize, unsigned int riOffset, char* rpCheck, char* rpPatch, int riPatchLength )
{

  if ( riOffset + riPatchLength > riFileSize )
  {
    return ERR_OVER_BUFFER;
  }

  fseek( rpFile, riOffset, SEEK_SET );

  char* lpBuf = new char[ riPatchLength ];
  FileTransferData( rpFile, lpBuf, riPatchLength, TRANSFER_READ );

  int liBufPos;
  for ( liBufPos = 0; liBufPos < riPatchLength; liBufPos++ )
  {

    if ( lpBuf[ liBufPos ] != rpCheck[ liBufPos ] )
    {
      delete[] lpBuf;
      return ERR_PATCHCHECK_FAILED;
    }

  }

  int liSeekResult;
  liSeekResult = fseek( rpFile, riOffset, SEEK_SET );
  if ( liSeekResult != 0 )
  {
    delete[] lpBuf;
    return ERR_CANT_SEEK;
  }

  if ( FileTransferData( rpFile, rpPatch, riPatchLength, TRANSFER_WRITE ) != ERR_OK )
  {
    delete[] lpBuf;
    return ERR_WRITE_ERROR;
  }


  delete[] lpBuf;
  return ERR_OK;

}


