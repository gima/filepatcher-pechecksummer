/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// File patcher and (If PE file, checksum fixer) v1.1

#include <cstdio>
#include <string.h>
#include <windows.h>

#include "Commons.h"


int main( int riArgCount, char** rpArgs )
{

  FILE* lpPatchFile     = 0;
  char* lpPatchFileBuf  = 0;
  FILE* lpPatchedFile   = 0;

  int liError           = ERR_OK;



  if ( riArgCount < 3 )
  {
    printf( "Usage: PE-Patcher-Checksum.exe <patch-file> <file-to-be-patched>\n" );
    liError = ERR_WRONG_ARGCOUNT;
    goto _Finished;
  }

  // Open patch file in read(binary) mode
  lpPatchFile = fopen( rpArgs[1], "rb" );
  if ( lpPatchFile == 0 )
  {
    printf( "Unable to open patch file" );
    liError = ERR_CANT_OPEN_PATCHFILE;
    goto _Finished;
  }

  // Get patch file size and quit if its less than length of "XX,XX,XX"
  unsigned int liPatchFileSize = FileGetSize( lpPatchFile );
  if ( liPatchFileSize < 8 )
  {
    printf( "Patch file size too small" );
    liError = ERR_SMALL_PATCHFILE;
    goto _Finished;
  }

  // Assign patch file buffer to be size of the file plus 1 for null, string termination character
  lpPatchFileBuf = new char[ liPatchFileSize + 1 ];
  lpPatchFileBuf[ liPatchFileSize ] = 0; // Ensure that last byte is 0 (end of text file)

  // Read patch file to buffer
  liError = FileTransferData( lpPatchFile, lpPatchFileBuf, liPatchFileSize, TRANSFER_READ );
  if ( liError != ERR_OK )
  {
    // this routine can only return ERR_NOT_ENOUGH_TRANSFERRED
    printf( "Requested to read whole patch file but only part of it was read\n" );
    goto _Finished;
  }


  // Open file to be patched
  lpPatchedFile = fopen( rpArgs[2], "r+b" );
  if ( lpPatchedFile == 0 )
  {
    printf( "Unable to open file to be patched\n" );
    liError = ERR_CANT_OPEN_PATCHEDFILE;
    goto _Finished;
  }

  // Get patch file size and quit if it 0
  unsigned int liPatchedFileSize = FileGetSize( lpPatchedFile );
  if ( liPatchedFileSize == 0 )
  {
    printf( "File to be patched is empty\n" );
    liError = ERR_PATCHEDFILE_EMPTY;
    goto _Finished;
  }

  // Here begins parsing of patch file


  // Assign temporary pointer to read patch file buffer so that it can be manipulated without losing the original buffer begin pos
  char* lpPatchFileBufPos = lpPatchFileBuf;

  // Get patch file text length (ye ye it's maybe the same as patch file size but just to be safe)
  int liPatchFileBufLength = strlen( lpPatchFileBuf );

  // Keep track of current line in patch file, beginning from 1
  int liPatchFileLine;
  liPatchFileLine = 1;

  while ( 1 ) // Loop each line
  {

    // If buffer is over length of patch file buffer, done parsing file
    if ( (int)lpPatchFileBufPos > (int)lpPatchFileBuf + liPatchFileBufLength )
    {
      break;
    }


    // Get end of line position, \r (line end == \r\n)
    char* lpEolCharPos;
    lpEolCharPos = strstr( lpPatchFileBufPos, "\r" );


    if ( lpEolCharPos == 0 )  // If end of line was not found, it's last line
    {
      // lpPatchFileBufPos is current line's begin position in any while loop, add to it the length of the current string
      // beginning from lpPatchFileBufPos and thus we get last line's null character's position as eolcharpos
      lpEolCharPos = lpPatchFileBufPos + strlen( lpPatchFileBufPos );
    }

    // Get current line's length from reducing current line begin pointer position from end of line character's position
    int liPatchLineLength = (int)lpEolCharPos - (int)lpPatchFileBufPos;

    // Check to see if line is empty or a comment, skip if it is so
    if ( (liPatchLineLength == 0) || (lpPatchFileBufPos[0] == ';') )
    {
      lpPatchFileBufPos += liPatchLineLength+2; // Next line is after "\r\n" which is 2 bytes
      liPatchFileLine++;
      continue;
    }


    // Variables to hold information for current line's extractions (part1,part2,part3)
    unsigned int liPatchOffset  = 0;  // Offset to apply the patch to, part1
    char* lpPatchCheck          = 0;  // Buffer to check before applying the patch, part2
    char* lpPatch               = 0;  // Buffer to apply, part3
    int liPatchLength           = 0;  // Length of patch buffers

    // Parse current line, get memory for pointer lpPatchCheck and lpPatch and fill values for liPatchOffset and liPatchLength
    liError = ParsePatchFileLine( lpPatchFileBufPos, liPatchLineLength, &liPatchOffset, &lpPatchCheck, &lpPatch, &liPatchLength );

    if ( liError != ERR_OK )
    {

      printf( "Parsing of patch file line %d failed: ", liPatchFileLine );

      switch ( liError )
      {

        case ERR_NO_FIRST_COMPONENT:
          printf( "'offset' not found\n" );
          break;
        case ERR_FIRSTCOMPONENT_LASTCOMPONENT:
          printf( "'patch check' and 'patch' missing\n" );
          break;
        case ERR_NO_SECOND_COMPONENT:
          printf( "no 'patch check' found\n" );
          break;
        case ERR_SECONDCOMPONENT_LASTCOMPONENT:
          printf( "'patch' missing\n" );
          break;
        case ERR_EMPTY_COMPONENT:
          printf( "'offset', 'patch check' or 'patch' is empty\n" );
          break;
        case ERR_PATCHES_NOT_MOD2:
          printf( "'offset', 'patch check' and 'patch' must be paired\n" );
          break;
        case ERR_CHECK_PATCH_DIFFERENT_LENGTH:
          printf( "length of 'patch check' and 'patch' must be same\n" );
          break;

        default:
          printf( "unknown error %d\n", liError );
          break;
      }

      lpPatchFileBufPos += liPatchLineLength+2; // Next line is after "\r\n" which is 2 bytes
      liPatchFileLine++;

      liError = ERR_OK;
      continue;

    }


    /*
    // Convert patch check buffer and patch buffer of length liPatchLength to text to display to user
    char* lpPatchCheckText  = 0;
    char* lpPatchText       = 0;

    ConvertBufferToText( lpPatchCheck, &lpPatchCheckText, liPatchLength );
    ConvertBufferToText( lpPatch, &lpPatchText, liPatchLength );

    printf( "offset 0x%x, check \"%s\", patch \"%s\"\n", liPatchOffset, lpPatchCheckText, lpPatchText );

    DeletePcharIfNecessary( lpPatchCheckText );
    DeletePcharIfNecessary( lpPatchText );
    */

    liError = PatchFile( lpPatchedFile, liPatchedFileSize, liPatchOffset, lpPatchCheck, lpPatch, liPatchLength );
    if ( liError != ERR_OK )
    {

      printf( "Patch from line %d couldn't be applied: ", liPatchFileLine );

      switch ( liError )
      {

        case ERR_OVER_BUFFER:
          printf( "patch would be written over file end (over buffer)\n" );
          break;

        case ERR_PATCHCHECK_FAILED:
          printf( "'patch check' didn't match\n" );
          break;

        case ERR_CANT_SEEK:
          printf( "can't seek to 'offset'\n" );
          break;

        case ERR_WRITE_ERROR:
          printf( "patch was not applied correctly, file corruption\n" );
          break;

      } // end of switch liError

      liError = ERR_OK;

    }

    DeletePcharIfNecessary( lpPatchCheck );
    DeletePcharIfNecessary( lpPatch );

    liPatchFileLine++;
    lpPatchFileBufPos += liPatchLineLength+2; // Next line is after "\r\n" which is 2 bytes

  } // End of loop each line


  // End of parse patch file


  // Close open file pointer, delete buffer and return liError
  _Finished:

  if ( lpPatchFile != 0 )
  {
    fclose( lpPatchFile );
  }

  if ( lpPatchedFile != 0 )
  {

    if ( liError == 0 )
    {

      liError = CorrectPe( lpPatchedFile, rpArgs[2] );

      if ( liError != ERR_OK )
      {
        switch ( liError )
        {
          case ERR_NOT_PE1:
          case ERR_NOT_PE2:
            liError = ERR_OK;
            break;
          case ERR_CANT_CORRECT_PE_CHECKSUM:
            printf( "Can't calculate pe checksum\n" );
            break;
          case ERR_CHECKSUM_NOT_ENTIRELY_WRITTEN:
            printf( "Pe checksum only partially written, corruption\n" );
            break;
        }

      } // if liError != ERR_OK

    } // if liError == 0

    fclose( lpPatchFile );

  }

  DeletePcharIfNecessary( lpPatchFileBuf );

  if ( liError == 0 )
  {
    printf( "Done patching \"%s\" with patch file \"%s\"..\n", rpArgs[2], rpArgs[1] );
  }

  return liError;

}


void DeletePcharIfNecessary( char* buf )
{

  if ( buf != 0 )
  {
    delete[] buf;
  }

}


