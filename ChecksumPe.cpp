/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <windows.h>
#include <imagehlp.h>

#include "Commons.h"


/*
0x0 == MZ (4D 5A)
0x3c == 4bytes offset to signature (+4 == pe-header begin)
signature == PE.. (50 45 00 00)
signature + 0x58 == checksum as 4 bytes

 PIMAGE_NT_HEADERS CheckSumMappedFile(
  PVOID BaseAddress,
  DWORD FileLength,
  PDWORD HeaderSum,
  PDWORD CheckSum
 );
*/


int CorrectPe( FILE* rpFile, char* rpFilename )
{

  int liError = ERR_OK;

  char lpPeBuf[4];
  //int liChecksumOffset;
  unsigned int liCorrectedChecksum;

  fseek( rpFile, 0, SEEK_SET );
  if ( FileTransferData( rpFile, &lpPeBuf[0], 2, TRANSFER_READ ) != ERR_OK )
  {
    liError = ERR_CANT_READ1;
    goto _Finished;
  }
  if ( ( lpPeBuf[0] != 'M' ) || ( lpPeBuf[1] != 'Z' ) )
  {
    liError = ERR_NOT_PE1;
    goto _Finished;
  }

  fseek( rpFile, 0x3c, SEEK_SET );
  if ( FileTransferData( rpFile, &lpPeBuf[0], 4, TRANSFER_READ ) != ERR_OK )
  {
    liError = ERR_CANT_READ2;
    goto _Finished;
  }

  fseek( rpFile, *((unsigned int*)&lpPeBuf), SEEK_SET );
  if ( FileTransferData( rpFile, &lpPeBuf[0], 4, TRANSFER_READ ) != ERR_OK )
  {
    liError = ERR_CANT_READ3;
    goto _Finished;
  }
  if ( *((unsigned int*)&lpPeBuf[0]) != 0x4550 )
  {
    liError = ERR_NOT_PE2;
    goto _Finished;
  }
  fseek( rpFile, 0x54, SEEK_CUR );
  //liChecksumOffset = ftell( lpPatchedFile );
  //if ( ReadFileData( lpPatchedFile, &lpPeBuf[0], 4 ) != 0 ) goto _NotPe;

  if ( MapFileAndCheckSum( rpFilename, (DWORD*)&lpPeBuf, (DWORD*)&liCorrectedChecksum ) != 0 )
  {
    liError = ERR_CANT_CORRECT_PE_CHECKSUM;
    goto _Finished;
  }

  //fseek( lpPatchedFile, 0x54, SEEK_CUR );
  if ( FileTransferData( rpFile, (char*)&liCorrectedChecksum, sizeof( liCorrectedChecksum ), TRANSFER_WRITE ) != ERR_OK )
  {
    liError = ERR_CHECKSUM_NOT_ENTIRELY_WRITTEN;
    goto _Finished;
  }
  //printf( "%.8x\n", *((unsigned int*)&lpPeBuf) );*/

  _Finished:

  return liError;
}


