/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cstdio>

enum {
  ERR_OK,

  // main
  ERR_WRONG_ARGCOUNT,
  ERR_CANT_OPEN_PATCHFILE,
  ERR_SMALL_PATCHFILE,
  ERR_CANT_OPEN_PATCHEDFILE,
  ERR_PATCHEDFILE_EMPTY,

  // FileTransferData
  ERR_NOT_ENOUGH_TRANSFERRED,

  // ParsePatchFileLine
  ERR_NO_FIRST_COMPONENT,
  ERR_FIRSTCOMPONENT_LASTCOMPONENT,
  ERR_NO_SECOND_COMPONENT,
  ERR_SECONDCOMPONENT_LASTCOMPONENT,
  ERR_EMPTY_COMPONENT,
  ERR_PATCHES_NOT_MOD2,
  ERR_CHECK_PATCH_DIFFERENT_LENGTH,

  // PatchFile

  ERR_OVER_BUFFER,
  ERR_PATCHCHECK_FAILED,
  ERR_CANT_SEEK,
  ERR_WRITE_ERROR,

  // CorrectPe
  ERR_CANT_READ1,
  ERR_NOT_PE1,
  ERR_CANT_READ2,
  ERR_CANT_READ3,
  ERR_NOT_PE2,
  ERR_CANT_CORRECT_PE_CHECKSUM,
  ERR_CHECKSUM_NOT_ENTIRELY_WRITTEN
};

enum {
  TRANSFER_READ,
  TRANSFER_WRITE
};

// main
void DeletePcharIfNecessary( char* buf );

// file routines
unsigned int FileGetSize( FILE* rpFile );
int FileTransferData( FILE* rpFile, char* rpBuf, int riSize, int riTransferMode );
int PatchFile( FILE* rpFile, unsigned int riFileSize, unsigned int riOffset, char* rpCheck, char* rpPatch, int riPatchLength );

// parse patch file line
int ParsePatchFileLine( char* rpBufLine, int riLineLength, unsigned int* riPatchOffset, char** rpPatchCheck,
  char** rpPatch, int* rpPatchLength );

// test routines
void ConvertBufferToText( char* rpInBuf, char** rpOutBuf, int riInBufLen );
void ConvertTextToBuf( char* rpOutBuf, char* rpInTextBuf, int rpOutBufLen );
unsigned char GetValueForHexChar( char rpHexChar );

// checksum pe
int CorrectPe( FILE* rpFile, char* rpFilename );


