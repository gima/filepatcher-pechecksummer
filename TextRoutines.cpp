/* Copyright 2006-2011 Gima

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string.h>

#include "Commons.h"

void ConvertBufferToText( char* rpInBuf, char** rpOutBuf, int riInBufLen )
{

  *rpOutBuf = new char[ (riInBufLen * 2)+1 ]; // Assign data for out buffer, length == inbuflength(as in bytes) times
                                              // length of one printed plus one null character to indicate string end
  memset( *rpOutBuf, 0, (riInBufLen * 2)+1 ); // Null above buffer

  char* lpOutBufHelper;       // Current position to append data to
  lpOutBufHelper = *rpOutBuf; // Initialize helper to outbuf begin

  int liInBufPos;
  for ( liInBufPos = 0; liInBufPos < riInBufLen; liInBufPos++ )
  {
    sprintf( lpOutBufHelper, "%.2x", (unsigned char)rpInBuf[ liInBufPos ] );  // Print to buffer, limited for safety
                                                                   // to current buf pos
    lpOutBufHelper += 2;  // Increase current buf pos by length of printed text

  }

}


void ConvertTextToBuf( char* rpOutBuf, char* rpInTextBuf, int rpOutBufLen )
{

  int liOutBufPos;
  short unsigned int liLowBits;
  short unsigned int liHighBits;

  // Loop each out buffer byte
  for ( liOutBufPos = 0; liOutBufPos < rpOutBufLen; liOutBufPos++ )
  {

    liHighBits = GetValueForHexChar( rpInTextBuf[ liOutBufPos*2 ] );        // Get 4 high bits from hex char
    liLowBits  = GetValueForHexChar( rpInTextBuf[ (liOutBufPos*2) + 1 ] );  // Get 4 low bits from hex char

    // Left shift high bits to their correct position (4 bits left) and or them with low == currect byte from 2 hex chars
    rpOutBuf[ liOutBufPos ] = ( liHighBits << 4 ) | liLowBits;

  }

}


unsigned char GetValueForHexChar( char rpHexChar )
{

  switch ( rpHexChar )
  {
    default:            return 0;
    case '1':           return 1;
    case '2':           return 2;
    case '3':           return 3;
    case '4':           return 4;
    case '5':           return 5;
    case '6':           return 6;
    case '7':           return 7;
    case '8':           return 8;
    case '9':           return 9;
    case 'A': case 'a': return 10;
    case 'B': case 'b': return 11;
    case 'C': case 'c': return 12;
    case 'D': case 'd': return 13;
    case 'E': case 'e': return 14;
    case 'F': case 'f': return 15;
  }

}


